// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SampleGameMode.generated.h"

/**
 * 
 */
UCLASS()
class UNREALCPPDEMO_API ASampleGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASampleGameMode();

	// Handles increasing of points and determining win condition
	void IncreasePoints();

	UPROPERTY(EditAnywhere, Category = "Game Rules")
	int32 PointsToWin;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Game Rules")
	int32 CurrentPoints;

protected:
	// Use a BlueprintImplementable event as a callback mechanism for the inheriting blueprint. We used this instead of BlueprintNativeEvent since we'll leave the entire implementation to the blueprint. We don't need to implement the function here
	UFUNCTION(BlueprintImplementableEvent)
	void OnPointsIncreased();
	UFUNCTION(BlueprintImplementableEvent)
	void OnWin();
};
