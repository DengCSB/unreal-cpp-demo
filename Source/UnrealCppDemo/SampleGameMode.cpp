// Fill out your copyright notice in the Description page of Project Settings.

#include "SampleGameMode.h"

ASampleGameMode::ASampleGameMode()
{
	// Initial value
	PointsToWin = 5;
}

void ASampleGameMode::IncreasePoints()
{
	CurrentPoints++;
	OnPointsIncreased();

	if (CurrentPoints >= PointsToWin)
	{
		OnWin();
	}
}
