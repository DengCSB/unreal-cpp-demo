// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealCppDemoGameMode.generated.h"

UCLASS(minimalapi)
class AUnrealCppDemoGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnrealCppDemoGameMode();
};



