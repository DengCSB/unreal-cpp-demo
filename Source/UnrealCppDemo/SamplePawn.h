// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SamplePawn.generated.h"

// Don't include anything here. Classes should be forward declared in the header file. Include the classes in the cpp file instead.

UCLASS()
class UNREALCPPDEMO_API ASamplePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASamplePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// This component is one of the many built-in movement components. We should always try to use what's already in the engine.
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UFloatingPawnMovement* Movement;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UStaticMeshComponent* StaticMesh;

	// Adding your own camera component allows you to override the default pawn camera. We want the camera to be behind of our pawn
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	// Camera can be attached to this so that when it detects collision it will adjust the camera's position.
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USpringArmComponent* CameraArm;

	// TSubclassOf is a way to store class/data type as a variable. We'll use this to define which ABullet to spawn
	UPROPERTY(EditAnywhere, Category = "Shooting")
	class TSubclassOf<class ABullet> BulletClass;

	// Defines the bullet spawn distance from the actor
	// BlueprintReadOnly gives access to the blueprint nodes.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooting")
	float BulletSpawnOffset;

	// Input bindings
	void MoveForward(float scale);
	void MoveRight(float scale);
	void Turn(float scale);
	void LookUp(float scale);
	void Shoot();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
