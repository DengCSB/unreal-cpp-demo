// Fill out your copyright notice in the Description page of Project Settings.

#include "SamplePawn.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/FloatingPawnMovement.h" // Search the class in Unreal API to get the exact directory of the header file
#include "GameFramework/SpringArmComponent.h"
#include "Bullet.h"

// Sets default values
ASamplePawn::ASamplePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Add static mesh component
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	// Set the static mesh as the root component
	SetRootComponent(StaticMesh);

	// Add floating pawn movement component
	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");

	// Setup camera arm
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(StaticMesh);
	// Initial length (can be modified in the blueprint)
	CameraArm->TargetArmLength = 500.0f;

	// Setup camera
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	// Attach the camera to the spring arm. This allows the camera to move with the spring arm
	Camera->SetupAttachment(CameraArm);

	// Setup the pawn so that it inherits the controller's rotation
	bUseControllerRotationYaw = true;
	bUseControllerRotationPitch = true;

	// Initial spawn offset
	BulletSpawnOffset = 500.0f;
}

// Called when the game starts or when spawned
void ASamplePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASamplePawn::MoveForward(float scale)
{
	// If we're not pressing anything, scale value is always 0. This means no movement will happen.
	Movement->AddInputVector(GetActorForwardVector() * scale);
}

void ASamplePawn::MoveRight(float scale)
{
	Movement->AddInputVector(GetActorRightVector() * scale);
}

void ASamplePawn::Turn(float scale)
{
	// Controller has its own rotation. We can use this to control the rotation of the camera
	AddControllerYawInput(scale);
}

void ASamplePawn::LookUp(float scale)
{
	AddControllerPitchInput(scale);
}

void ASamplePawn::Shoot()
{
	// Only spawn a bullet if the class is set
	if (!BulletClass) return;

	// Spawn the bullet

	// Setup spawn parameters
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this; // Responsible for spawning this object
	spawnParams.Instigator = this; // Responsible for damage (if you need it)

	// Setup transform
	FTransform transform;
	transform.SetLocation(GetActorForwardVector() * BulletSpawnOffset + GetActorLocation()); // Alternatively, you can use an Arrow component
	transform.SetRotation(GetActorRotation().Quaternion());
	transform.SetScale3D(FVector(1.0f));

	GetWorld()->SpawnActor<ABullet>(BulletClass, transform, spawnParams);
}

// Called every frame
void ASamplePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASamplePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// This allows MoveForward/MoveRight function to get called upon receiving input. This works by sending a pointer to the function of our class to the Input System
	PlayerInputComponent->BindAxis("MoveForward", this, &ASamplePawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASamplePawn::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &ASamplePawn::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &ASamplePawn::LookUp);

	// Bind the Fire button to the Shoot function. This will only register when button is pressed once (IE_Pressed)
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASamplePawn::Shoot);
}

